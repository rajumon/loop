<?php
    // // ascending order 
    // for ($x=1; $x <= 5; $x++){ 
    //     echo $x."</br>";
    // }

    // // decrementing order
    // for( $x=5; $x>=1; $x-- ){
    //     echo $x."<br>";
    // }

    //increment operator
    // for($x=1; $x <=5;){ 
    //     echo $x."<br>";
    //     $x++;
    // }

    //increment by ten
    // for($x=10; $x<=50;){
    //     echo $x."<br>"; 
    //     $x= $x + 10;
    // }

    // array using for loop
    // $arr_variable = array (10,20,30,40,50);

    // for ($i=0; $i<count($arr_variable); $i++){
    //     echo "The array item is: $arr_variable[$i] <br />";
    // }

    //string  array using for loop
    // $arr_course = array();
    // $arr_course[0] = "PHP";
    // $arr_course[1] = "MySQL";
    // $arr_course[2] = "Java";
 
    // for ($i=0; $i<count($arr_course); $i++){
    //     echo "The element" . $i .":" .$arr_course[$i] ."<br />";
    // }


    // for ($i=0; $i <=5 ; $i++) { 
    //     echo "<br>";
    //     echo "value of i is :".$i;
    // }

    // $i=1;
    // for (; $i <=5 ; $i++) { 
    //     echo "<br>";
    //     echo "value of i is :".$i;
    // }

    // for ($i=1;  ;$i++) { 
    //     if($i == 4){
    //         break;
    //     }
    //     echo "<br>";
    //     echo "value of i is :".$i;
    // }

    // for ($i=1; $i <10;) { 
    //     if ($i== 7) {
    //         break;
    //     }
    //     echo "<br>";
    //     echo "Value of i is :".$i;
    //     $i++;
    // }

    // $i=1;
    // for (; ;) { 
    //     if ($i == 8) {
    //         break;
    //     }
    //     echo "<br>";
    //     echo "Value of i is :".$i;
    //     $i++;
    // }

    
    // $fruits = array('orange', 'banana', 'papaya', 'strawberry');
    // $count = count($fruits);

    // for ($i=0; $i < $count ; $i++) { 
    //     echo "<br>";
    //     echo  'Fruit Name : '.$fruits[$i];
    // }

    // for ($i=0; $i <=5 ; $i++) { 
    //     for ($j=0; $j <=$i ; $j++) { 
    //         echo " * ";
    //     }
    //     echo "<br>";
    // }

    $directions = array('east','west','north', 'south');
    foreach($directions as $key=>$value) {
        echo 'Direction =>'. $value.'<br/>';
    }
?>